<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\Models\Vendor;
use App\Models\Members;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/**
 * User Factory
 */
$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

/**
 * Vendor Factory
 */
$factory->define(Vendor::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'telephone' => $faker->e164PhoneNumber,
        'address' => $faker->address,
    ];
});

/**
 * Member Factory
 */
$factory->define(Members::class, function (Faker $faker) {
    $first_name = $faker->firstName;
    $last_name = $faker->lastName;
    return [
        'full_name' => $first_name . " " . $last_name,
        'nick_name'=> $first_name,
        'email' => $faker->safeEmail,
        'is_verified' => $faker->randomElement($array = array ('y','n')),
        'bio' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'vendor_id' => Vendor::pluck('id')->random()
    ];
});