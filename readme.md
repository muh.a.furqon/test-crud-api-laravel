# Laravel API CRUD buat tes

## Petunjuk

1. Clone repository
2. buat database baru
3. setting database di file .env (host, user, password, nama database)
4. jalankan perintah composer install
5. jalankan perintah php artisan migrate
6. jalankan perintah php artisan db:seed
7. jalankan perintah php artisan serve

link postman : https://www.getpostman.com/collections/be3e9fddeb07d3824106
