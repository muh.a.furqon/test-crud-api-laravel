<div class="table-responsive">
    <table class="table" id="members-table">
        <thead>
            <tr>
                <th>Full Name</th>
        <th>Nick Name</th>
        <th>Email</th>
        <th>Is Verified</th>
        <th>Bio</th>
        <th>Vendor Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($members as $members)
            <tr>
                <td>{!! $members->full_name !!}</td>
            <td>{!! $members->nick_name !!}</td>
            <td>{!! $members->email !!}</td>
            <td>{!! $members->is_verified !!}</td>
            <td>{!! $members->bio !!}</td>
            <td>{!! $members->vendor_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['members.destroy', $members->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('members.show', [$members->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('members.edit', [$members->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
