<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $members->id !!}</p>
</div>

<!-- Full Name Field -->
<div class="form-group">
    {!! Form::label('full_name', 'Full Name:') !!}
    <p>{!! $members->full_name !!}</p>
</div>

<!-- Nick Name Field -->
<div class="form-group">
    {!! Form::label('nick_name', 'Nick Name:') !!}
    <p>{!! $members->nick_name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $members->email !!}</p>
</div>

<!-- Is Verified Field -->
<div class="form-group">
    {!! Form::label('is_verified', 'Is Verified:') !!}
    <p>{!! $members->is_verified !!}</p>
</div>

<!-- Bio Field -->
<div class="form-group">
    {!! Form::label('bio', 'Bio:') !!}
    <p>{!! $members->bio !!}</p>
</div>

<!-- Vendor Id Field -->
<div class="form-group">
    {!! Form::label('vendor_id', 'Vendor Id:') !!}
    <p>{!! $members->vendor_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $members->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $members->updated_at !!}</p>
</div>

