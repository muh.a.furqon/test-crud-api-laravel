<?php

namespace App\Repositories;

use App\Models\Members;
use App\Repositories\BaseRepository;

/**
 * Class MembersRepository
 * @package App\Repositories
 * @version July 6, 2019, 1:12 am UTC
*/

class MembersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'full_name',
        'nick_name',
        'email',
        'is_verified',
        'bio',
        'vendor_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Members::class;
    }
}
