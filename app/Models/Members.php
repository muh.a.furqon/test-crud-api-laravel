<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Members
 * @package App\Models
 * @version July 6, 2019, 1:12 am UTC
 *
 * @property string full_name
 * @property string nick_name
 * @property string email
 * @property string is_verified
 * @property string bio
 * @property integer vendor_id
 */
class Members extends Model
{
    use SoftDeletes;

    public $table = 'members';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'full_name',
        'nick_name',
        'email',
        'is_verified',
        'bio',
        'vendor_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'full_name' => 'string',
        'nick_name' => 'string',
        'email' => 'string',
        'is_verified' => 'string',
        'bio' => 'string',
        'vendor_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'full_name' => 'required',
        'email' => 'required',
        'is_verified' => 'required',
        'vendor_id' => 'required'
    ];

    
}
