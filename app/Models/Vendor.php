<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vendor
 * @package App\Models
 * @version July 6, 2019, 00:31 pm UTC+8
 *
 * @property string name
 * @property string telephone
 * @property string address
 */
class Vendor extends Model
{
    use SoftDeletes;

    public $table = 'vendors';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'telephone',
        'address'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'telephone' => 'string',
        'address' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    
}
