<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeMembersTrait;
use Tests\ApiTestTrait;

class MembersApiTest extends TestCase
{
    use MakeMembersTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_members()
    {
        $members = $this->fakeMembersData();
        $this->response = $this->json('POST', '/api/members', $members);

        $this->assertApiResponse($members);
    }

    /**
     * @test
     */
    public function test_read_members()
    {
        $members = $this->makeMembers();
        $this->response = $this->json('GET', '/api/members/'.$members->id);

        $this->assertApiResponse($members->toArray());
    }

    /**
     * @test
     */
    public function test_update_members()
    {
        $members = $this->makeMembers();
        $editedMembers = $this->fakeMembersData();

        $this->response = $this->json('PUT', '/api/members/'.$members->id, $editedMembers);

        $this->assertApiResponse($editedMembers);
    }

    /**
     * @test
     */
    public function test_delete_members()
    {
        $members = $this->makeMembers();
        $this->response = $this->json('DELETE', '/api/members/'.$members->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/members/'.$members->id);

        $this->response->assertStatus(404);
    }
}
