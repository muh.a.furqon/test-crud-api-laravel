<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Members;
use App\Repositories\MembersRepository;

trait MakeMembersTrait
{
    /**
     * Create fake instance of Members and save it in database
     *
     * @param array $membersFields
     * @return Members
     */
    public function makeMembers($membersFields = [])
    {
        /** @var MembersRepository $membersRepo */
        $membersRepo = \App::make(MembersRepository::class);
        $theme = $this->fakeMembersData($membersFields);
        return $membersRepo->create($theme);
    }

    /**
     * Get fake instance of Members
     *
     * @param array $membersFields
     * @return Members
     */
    public function fakeMembers($membersFields = [])
    {
        return new Members($this->fakeMembersData($membersFields));
    }

    /**
     * Get fake data of Members
     *
     * @param array $membersFields
     * @return array
     */
    public function fakeMembersData($membersFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'full_name' => $fake->word,
            'nick_name' => $fake->word,
            'email' => $fake->word,
            'is_verified' => $fake->randomElement(['yes:y', 'no:n']),
            'bio' => $fake->word,
            'vendor_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $membersFields);
    }
}
