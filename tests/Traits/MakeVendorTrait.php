<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Vendor;
use App\Repositories\VendorRepository;

trait MakeVendorTrait
{
    /**
     * Create fake instance of Vendor and save it in database
     *
     * @param array $vendorFields
     * @return Vendor
     */
    public function makeVendor($vendorFields = [])
    {
        /** @var VendorRepository $vendorRepo */
        $vendorRepo = \App::make(VendorRepository::class);
        $theme = $this->fakeVendorData($vendorFields);
        return $vendorRepo->create($theme);
    }

    /**
     * Get fake instance of Vendor
     *
     * @param array $vendorFields
     * @return Vendor
     */
    public function fakeVendor($vendorFields = [])
    {
        return new Vendor($this->fakeVendorData($vendorFields));
    }

    /**
     * Get fake data of Vendor
     *
     * @param array $vendorFields
     * @return array
     */
    public function fakeVendorData($vendorFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'telephone' => $fake->word,
            'address' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $vendorFields);
    }
}
